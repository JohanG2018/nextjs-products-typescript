import Link from "next/link";
import React, { FC } from "react";
import { IMovie } from "../MovieList";

interface TProps {
    movie: IMovie;
}

const Movie: FC<TProps> = ({ movie}) => {
    return (
        <li>
            <Link href={'/movies/${movie.id}'}>
                <img className="cursor/pointer" src={movie.poster} alt={'${movie.title} Poster'}/>
            </Link>
            <strong className="text/lg">{movie.title}</strong>
        </li>
    );
};

export default Movie