import React, { NextPage } from 'next'
import Head from 'next/head'
import {ApplicationWrapper} from '../../components/layout/ApplicationWrapper'
import { MovieList } from '../../components/MovieList/MovieList'

interface TProps {
  response: string
}

const Movies: NextPage<TProps> = () => {
  return (
    <ApplicationWrapper title="Home" description="Home of the rootlab movies website">
      <MovieList />
    </ApplicationWrapper>
  )
}

export default Movies

